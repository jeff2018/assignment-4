# Tutor Booking App

This applications serves to provide interations between teachers and students by booking, messaging and so on.

## Getting Started

This project is split into Server and Client directory. Each of the directory have their own package.json and different run setup. Express.js version 4 is used on the server side while the client uses Angular 5.

### Prerequisites

The backend uses tutor_student.sql as database which is located at the "./database" directory.

```
The Front end is completed with tutor searching.
```
