require('dotenv').config()
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const NODE_PORT = process.env.PORT;
const { findAll, findOne, saveOne, searchTutors, searchById, searchByTutorName, deleteOne,
    searchByTutorSubject, searchByTopics, searchByFacePhoto, searchByFirstLesson,
    searchByLastLesson, searchByHourlyRates, searchByStudentCount, updateOne } = require("./app-sql.js");
app.use(cors())
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.get("/api/tutors", function (req, res) {
    console.log(req.query.limit, req.query.offset);
    let limit = parseInt(req.query.limit) || 50;
    let offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset]).then(function (results) {
        res.status(200).json(results);
    }).catch(function (err) {
        res.status(500).end();
        console.log(err);
    });
});

app.get("/api/tutor/:tutorId", function (req, res) {
    console.log(req.params.tutorId);
    findOne([req.params.tutorId]).then(function (results) {
        res.status(200).json(results[0]);
    }).catch(function (err) {
        res.status(500).end();
        console.log(err);
    });
});

app.post("/api/tutors", function (req, res) {
    console.log(req.body);
    saveOne([req.body.tutor_name, req.body.tutor_subjects, req.body.topics,
    req.body.face_photo, req.body.first_lesson_date, req.body.last_lesson_date,
    req.body.hourly_rates, req.body.student_count]).then(function (results) {
        res.status(200).json(results);
        console.log(results);
    }).catch(function (err) {
        res.status(500).end();
        console.log(err);
    });
});

app.delete("/api/tutors/:delId", function (req, res) {
    console.log(req.body);
    res.status(200).json({});
    console.log(req.params.delId);
    deleteOne([req.params.delId])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.put("/api/tutors", function (req, res) {
    console.log(req.body);
    updateOne([req.body.tutor_name, req.body.tutor_subjects, req.body.topics,
    req.body.face_photo, req.body.first_lesson_date, req.body.last_lesson_date,
    req.body.hourly_rates, req.body.student_count, req.body.id]).then(function (result) {
        console.log(result);
        res.status(200).json(result);
    }).catch(function (err) {
        console.log(err);
        res.status(500).end();
    });
});

app.get("/api/tutors/search", function (req, res) {
    console.log(req.query.searchType);
    let searchType = req.query.searchType;
    (searchType !== 'all') ? sortBy = searchType : sortBy = 'id';
    let keyword = '%' + req.query.keyword + '%';
    let searchBy = searchTutors([keyword]);
    switch (searchType) {
        case 'all': let k = keyword;
            searchBy = searchTutors([k, k, k, k, k, k, k, k, k, sortBy]);
            break;
        case 'id': searchBy = searchById([keyword, sortBy]);
            break;
        case 'tutor_name': searchBy = searchByTutorName([keyword, sortBy]);
            break;
        case 'tutor_subjects': searchBy = searchByTutorSubject([keyword, sortBy]);
            break;
        case 'topics': searchBy = searchByTopics([keyword, sortBy]);
            break;
        case 'face_photo': searchBy = searchByFacePhoto([keyword, sortBy]);
            break;
        case 'first_lesson_date': searchBy = searchByFirstLesson([keyword, sortBy]);
            break;
        case 'last_lesson_date': searchBy = searchByLastLesson([keyword, sortBy]);
            break;
        case 'hourly_rates': searchBy = searchByHourlyRates([keyword, sortBy]);
            break;
        case 'student_count': searchBy = searchByStudentCount([keyword, sortBy]);
            break;
    }
    searchBy.then(function (results) {
        res.status(200).json(results);
    }).catch(function (err) {
        console.log(err);
        res.status(500).end();
    });
});
app.use(express.static(__dirname + "/public"));
app.listen(NODE_PORT, () => console.info("App server started on port " + NODE_PORT));