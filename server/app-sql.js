require('dotenv').config()
const mysql = require("mysql");
const q = require("q");

let pool = mysql.createPool({
    host: process.env.MYSQL_SERVER,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: process.env.MYSQL_CONNECTION
});

const makeQuery = (sql, pool) => {
    console.log(sql);
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};

const findAllTutors = "SELECT * FROM tutors LIMIT ? OFFSET ?;";
let findAll = makeQuery(findAllTutors, pool);

const findOneTutor = "SELECT * FROM tutors WHERE id = ?;";
let findOne = makeQuery(findOneTutor, pool);

const deleteOneTutor = "DELETE FROM tutors WHERE id = ?";
var deleteOne = makeQuery(deleteOneTutor, pool);

const updateTutor = `UPDATE tutors SET tutor_name = ?, tutor_subjects = ?, 
    topics = ?, face_photo = ?, first_lesson_date = ?, 
    last_lesson_date = ?, hourly_rates = ?, student_count = ? WHERE id = ?;`;
let updateOne = makeQuery(updateTutor, pool);

const saveOneTutor = `INSERT INTO tutors (tutor_name, tutor_subjects, 
    topics, face_photo, first_lesson_date, 
    last_lesson_date, hourly_rates, student_count) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;
let saveOne = makeQuery(saveOneTutor, pool);

const searchTutorsByAll = `SELECT * FROM tutors WHERE id LIKE ? || tutor_name LIKE ? || 
    tutor_subjects LIKE ? || topics LIKE ? || face_photo LIKE ? || 
    first_lesson_date LIKE ? || last_lesson_date LIKE ? || 
    hourly_rates LIKE ? || student_count LIKE ? ORDER BY ?;`;
const searchTutors = makeQuery(searchTutorsByAll, pool);

const searchTutorsById = `SELECT * FROM tutors WHERE id LIKE ? ORDER BY ?;`;
const searchById = makeQuery(searchTutorsById, pool);

const searchTutorsByTutorName = `SELECT * FROM tutors WHERE tutor_name LIKE ? ORDER BY ?;`;
const searchByTutorName = makeQuery(searchTutorsByTutorName, pool);

const searchTutorsByTutorSubject = `SELECT * FROM tutors WHERE tutor_subjects LIKE ? ORDER BY ?;`;
const searchByTutorSubject = makeQuery(searchTutorsByTutorSubject, pool);

const searchTutorsByTopics = `SELECT * FROM tutors WHERE topics LIKE ? ORDER BY ?;`;
const searchByTopics = makeQuery(searchTutorsByTopics, pool);

const searchTutorsByFacePhoto = `SELECT * FROM tutors WHERE face_photo LIKE ? ORDER BY ?;`;
const searchByFacePhoto = makeQuery(searchTutorsByFacePhoto, pool);

const searchTutorsByFirstLesson = `SELECT * FROM tutors WHERE first_lesson_date LIKE ? ORDER BY ?;`;
const searchByFirstLesson = makeQuery(searchTutorsByFirstLesson, pool);

const searchTutorsByLastLesson = `SELECT * FROM tutors WHERE last_lesson_date LIKE ? ORDER BY ?;`;
const searchByLastLesson = makeQuery(searchTutorsByLastLesson, pool);

const searchTutorsByHourlyRates = `SELECT * FROM tutors WHERE hourly_rates LIKE ? ORDER BY ?;`;
const searchByHourlyRates = makeQuery(searchTutorsByHourlyRates, pool);

const searchTutorsByStudentCount = `SELECT * FROM tutors WHERE student_count LIKE ? ORDER BY ?;`;
const searchByStudentCount = makeQuery(searchTutorsByStudentCount, pool);

module.exports = {
    findAll, findOne, saveOne, searchTutors, searchById, searchByTutorName, 
    searchByTutorSubject, searchByTopics, searchByFacePhoto, searchByFirstLesson,
    searchByLastLesson, searchByHourlyRates, searchByStudentCount, updateOne, deleteOne
};