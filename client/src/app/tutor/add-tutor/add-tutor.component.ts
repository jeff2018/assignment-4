import { Component, OnInit } from '@angular/core';
import { Tutor } from '../../shared/models/tutor';
import { TutorService } from '../../shared/services/tutor.service';
import { Observable } from 'rxjs/Observable';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { AuthService } from '../../shared/services/auth.service';

//Date need to be declared as empty in order for the model parameter to work.
const d: Date = new Date;

@Component({
  selector: 'app-add-tutor',
  templateUrl: './add-tutor.component.html',
  styleUrls: ['./add-tutor.component.css']
})
export class AddTutorComponent implements OnInit {

  tutorsObservable: Observable<Tutor>;
  model = new Tutor(1, '', '', '', '', d, d, 10, 10, 0);

  constructor(private tutorService: TutorService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private authService: AuthService) { }

  ngOnInit() {
    this.authService.setLogon(true);
  }

  onSaveTutor() {
    console.log("Add new tutor !");
    console.log(this.model);
    this.tutorsObservable = this.tutorService.addTutor(this.model)
      .map(result => result);
    this.tutorsObservable.subscribe(tutor => this.addToastMessage("Added tutor.", this.model.tutor_name));
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 4500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Tutor ' + toast.id + ' has been added!');
      }
    };
    this.toastyService.success(toastOptions);
  }
}