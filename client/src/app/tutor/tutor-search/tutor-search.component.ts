import { Component, OnInit, TemplateRef } from '@angular/core';
import { TutorCriteria } from '../../shared/models/tutor-criteria';
import { TutorResult } from '../../shared/models/tutor-result';
import { TutorService } from '../../shared/services/tutor.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Tutor } from '../../shared/models/tutor';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { AuthService } from '../../shared/services/auth.service';

@Component({
    selector: 'app-tutor-search',
    templateUrl: './tutor-search.component.html',
    styleUrls: ['./tutor-search.component.css']
})
export class TutorSearchComponent implements OnInit {
    searchTypes = [
        { desc: "All Fields", value: "all" }, { desc: "Tutor ID", value: "id" },
        { desc: "Tutor Name", value: "tutor_name" }, { desc: "Tutor Subjects", value: "tutor_subjects" },
        { desc: "Topics", value: "topics" }, { desc: "Photo of Tutor", value: "face_photo" },
        { desc: "Lesson Start Date", value: "first_lesson_date" }, { desc: "Lesson End Date", value: "last_lesson_date" },
        { desc: "Cost Per Hour", value: "hourly_rates" }, { desc: "Students Per Class", value: "student_count" }
    ];
    tutorsObservable$: Observable<TutorResult[]>;
    updateTutorObservable$: Observable<Tutor>;
    deleteTutorObservable$: Observable<Tutor>;
    modalRef: BsModalRef;
    result: TutorResult[] = [];
    private editTutor: Tutor;
    maxSize: number = 5;
    totalItems: number = 0;
    currentPage: number = 1;
    numPages: number = 0;
    inited: boolean = false;
    itemsPerPage: number = 10;
    indexOnPage: number = 10;
    model = new TutorCriteria('1', 'all', this.currentPage, this.itemsPerPage);
    perPage: number = 10;

    constructor(private tutorService: TutorService, private modalService: BsModalService,
        private toastyService: ToastyService, private toastyConfig: ToastyConfig, private authService: AuthService) {
        this.tutorsObservable$ = this.tutorService.searchTutors(this.model);
    }

    ngOnInit() {
        this.authService.setLogon(true);
        this.tutorsObservable$.subscribe((x) => {
            this.totalItems = x.length;
            this.result = x.slice(this.indexOnPage, this.model.itemsPerPage);
        });
    }

    onSearch() {
        console.log(this.model);
        this.tutorsObservable$ = this.tutorService.searchTutors(this.model)
            .do(result => this.totalItems = result.length)
            .map(result => result);
        this.tutorsObservable$.subscribe(tutors => this.result = tutors);
        this.perPage = this.model.itemsPerPage;
        this.pageChanged({ page: 1, itemsPerPage: this.model.itemsPerPage });
    }

    pageChanged(event): void {
        console.log(event);
        console.log('Page changed to: ' + event.page + ', Number items per page: ' + event.itemsPerPage);
        this.model.currentPerPage = event.page, this.model.itemsPerPage = event.itemsPerPage;
        this.indexOnPage = event.page * (this.perPage);
        this.tutorsObservable$ = this.tutorService.searchTutors(this.model)
            .do(result => {
                this.totalItems = result.length;
                const numPages = result.length / this.model.itemsPerPage;
                console.log(numPages);
                if (numPages > 1 && this.model.currentPerPage > 1) {
                    const startIndex = (this.indexOnPage - this.perPage);
                    console.log(this.indexOnPage);
                    const endIndex = this.indexOnPage;
                    console.log(`<< ${startIndex} < ${endIndex}`);
                    this.result = result.slice(startIndex, endIndex);
                    console.log(this.result);
                } else {
                    console.log('page 1  > ' + event.page + this.result);
                    this.result = result.slice(0, this.model.itemsPerPage);
                }
                return this.result;
            })
            .map(result => result);
        this.tutorsObservable$.subscribe();
        console.log('this.tutorsObservable: ' + this.tutorsObservable$);
    }


    edit(bkresult: TutorResult, template: TemplateRef<any>, index) {
        console.log(bkresult);
        this.editTutor = new Tutor(bkresult.id, bkresult.tutor_name, bkresult.tutor_subjects,
            bkresult.topics, bkresult.face_photo, bkresult.first_lesson_date,
            bkresult.last_lesson_date, bkresult.hourly_rates, bkresult.student_count, index);
        this.modalRef = this.modalService.show(template);
    }

    delete(bkresult: TutorResult, template: TemplateRef<any>, index) {
        console.log(bkresult);
        this.editTutor = new Tutor(bkresult.id, bkresult.tutor_name, bkresult.tutor_subjects,
            bkresult.topics, bkresult.face_photo, bkresult.first_lesson_date,
            bkresult.last_lesson_date, bkresult.hourly_rates, bkresult.student_count, index);
        this.modalRef = this.modalService.show(template);
    }

    onSaveEditTutor() {
        console.log("Save Edited Tutor");
        this.updateTutorObservable$ = this.tutorService.updateTutor(this.editTutor);
        this.updateTutorObservable$.subscribe(tutor => {
            this.addToastMessage("Update Tutor.", this.editTutor.index);
            let tutorRsObj = this.result[this.editTutor.index];
            tutorRsObj.id = this.editTutor.id, tutorRsObj.tutor_name = this.editTutor.tutor_name;
            tutorRsObj.tutor_subjects = this.editTutor.tutor_subjects, tutorRsObj.topics = this.editTutor.topics;
            tutorRsObj.face_photo = this.editTutor.face_photo, tutorRsObj.first_lesson_date = this.editTutor.first_lesson_date;
            tutorRsObj.last_lesson_date = this.editTutor.last_lesson_date, tutorRsObj.hourly_rates = this.editTutor.hourly_rates;
            tutorRsObj.student_count = this.editTutor.student_count;
            this.result[this.editTutor.index] = tutorRsObj;
            this.modalRef.hide();
        });
    }

    onDelTutor() {
        console.log("Delete tutor !" + this.editTutor.index);
        this.deleteTutorObservable$ = this.tutorService.deleteTutor(this.editTutor);
        this.deleteTutorObservable$.subscribe(tutor => {
            this.addToastMessage("Tutor deleted.", this.editTutor.tutor_name);
            console.log(this.editTutor.index);
            var tutorRsObj = this.result[this.editTutor.index];
            var index = this.result.indexOf(tutorRsObj, 0);
            if (index > -1) {
                this.result.splice(index, 1);
            }
            console.log(this.result[this.editTutor.index]);
            this.modalRef.hide();
        });
    }

    addToastMessage(title, msg) {
        let toastOptions: ToastOptions = {
            title: title, msg: msg, showClose: true,
            timeout: 11111, theme: 'bootstrap',
            onAdd: (toast: ToastData) => {
                console.log('Tutor ' + toast.id + ' has been updated!');
            }
        };
        this.toastyService.success(toastOptions);
    }
}