import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { TutorfirebaseService } from '../../shared/services/tutorfirebase.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
  selector: 'app-tutor-details',
  templateUrl: './tutor-details.component.html',
  styleUrls: ['./tutor-details.component.css']
})
export class TutorDetailsComponent implements OnInit {

  form: FormGroup;
  tocs = new Array();
  constructor(private authService: AuthService,
    private fb: FormBuilder,
    private tutorfirebase: TutorfirebaseService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig) {
    this.form = this.fb.group({
      longDescription: ['', Validators.required],
      toc: [''],
      email: ['', Validators.required],
      title: ['', Validators.required],

    });
  }

  ngOnInit() {
    this.authService.setLogon(true);
  }

  isErrorVisible(field: string, error: string) {

    return this.form.controls[field].dirty
      && this.form.controls[field].errors &&
      this.form.controls[field].errors[error];

  }

  addToc(form) {
    this.tocs.push(form.value.toc);
  }

  saveTutorDetails(form) {
    form.value.tocs = this.tocs;
    this.tutorfirebase.createNewTutorDetails(form.value);
    this.addToastMessage("Added tutor.", form.value.title)
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 4500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Tutor ' + toast.id + ' has been added!');
      }
    };
    this.toastyService.success(toastOptions);
  }

}
