import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TutorSearchComponent } from './tutor-search/tutor-search.component';
import { TutorDetailsComponent } from './tutor-details/tutor-details.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  BsDropdownModule, AlertModule, AccordionModule, ButtonsModule,
  CollapseModule, PopoverModule, ProgressbarModule, PaginationModule,
  ModalModule, TabsModule, CarouselModule
} from 'ngx-bootstrap';
import { HomeComponent } from '../home/home.component';
import { AddTutorComponent } from './add-tutor/add-tutor.component';
import { TutorUploadComponent } from './tutor-upload/tutor-upload.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TutorListComponent } from './tutor-list/tutor-list.component';

const tutorsRouting: ModuleWithProviders = RouterModule.forChild([
  { path: 'tutors/add', component: AddTutorComponent },
  { path: 'tutors/details', component: TutorDetailsComponent },
  { path: 'tutors/list', component: TutorListComponent },
  { path: 'tutors', component: TutorSearchComponent },
  { path: 'tutors-details/:tutor_id', component: TutorDetailsComponent },
  { path: 'tutor-upload', component: TutorUploadComponent }
]);

@NgModule({
  imports: [
    CommonModule, SharedModule, tutorsRouting, ReactiveFormsModule,
    ModalModule.forRoot(), PaginationModule.forRoot(),
    TabsModule.forRoot(), AccordionModule.forRoot(),
    AlertModule.forRoot(), ButtonsModule.forRoot(),
    CarouselModule.forRoot(), CollapseModule.forRoot(),
    BsDropdownModule.forRoot(), PopoverModule.forRoot(),
    ProgressbarModule.forRoot(), BrowserAnimationsModule
  ],
  declarations: [TutorSearchComponent, TutorDetailsComponent,
    AddTutorComponent, TutorUploadComponent, TutorUploadComponent, TutorListComponent]
})
export class TutorModule { }