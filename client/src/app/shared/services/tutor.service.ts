import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { TutorCriteria } from '../../shared/models/tutor-criteria';
import { TutorResult } from '../../shared/models/tutor-result';
import { Tutor } from '../models/tutor';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TutorService {
  private tutorRootApiUrl = `${environment.ApiUrl}/api/tutors`;
  private serachTutorApiURL = this.tutorRootApiUrl + "/search";

  constructor(private httpClient: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig) { }

  public addTutor(tutor: Tutor): Observable<Tutor> {
    console.log(tutor);
    console.log(this.tutorRootApiUrl);
    return this.httpClient.post<Tutor>(this.tutorRootApiUrl, tutor, httpOptions)
      .pipe(
        catchError(this.handleError('addTutor', tutor))
      );
  }

  public updateTutor(tutor: Tutor): Observable<Tutor> {
    console.log(tutor);
    console.log(this.tutorRootApiUrl);
    return this.httpClient.put<Tutor>(this.tutorRootApiUrl, tutor, httpOptions)
      .pipe(catchError(this.handleError('updateTutor', tutor)));
  }


  public deleteTutor(tutor: Tutor): Observable<Tutor> {
    console.log(tutor);
    console.log(tutor.id);
    console.log(this.tutorRootApiUrl);
    const deleteUrl = `${this.tutorRootApiUrl}/${tutor.id}`; // DELETE api/tutors/1
    console.log(deleteUrl);
    return this.httpClient.delete<Tutor>(deleteUrl, httpOptions)
      .pipe(
        catchError(this.handleError('deleteTutor', tutor))
      );
  }

  public searchTutors(model): Observable<TutorResult[]> {
    //let getURL = `${this.tutorApisURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}&currentPerPage=${model.currentPerPage}&itemsPerPage=${model.itemsPerPage}`;
    let getURL = `${this.serachTutorApiURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}`;
    return this.httpClient.get<TutorResult[]>(getURL, httpOptions)
      .pipe(catchError(this.handleError<TutorResult[]>('searchTutors')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.addToastMessage("Error", JSON.stringify(error.error));
      return Observable.throw(error || 'backend server error');
    };
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 11111,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: function (toast: ToastData) {
        console.log('Toast ' + toast.id + ' has been removed!');
      }
    };
    this.toastyService.error(toastOptions);
  }
}