import { Injectable, Inject } from '@angular/core';
import { Observable, Subject } from "rxjs/Rx";
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Tutordetails } from '../../shared/models/tutordetails';
import { UUID } from 'angular2-uuid';

@Injectable()
export class TutorfirebaseService {
  private tutorDetailsDoc: AngularFirestoreDocument<Tutordetails>;
  private tutordetailsCollection: AngularFirestoreCollection<Tutordetails>;
  item: Observable<Tutordetails>;
  constructor(private db: AngularFirestore) {
  }

  createNewTutorDetails(tutordetails: any) {
    const tutorDetailsToSave = Object.assign({}, tutordetails);
    let uuid = UUID.UUID();
    tutorDetailsToSave._id = uuid;
    this.tutorDetailsDoc = this.db.doc<Tutordetails>('tutordetails/' + tutorDetailsToSave.email);
    this.item = this.tutorDetailsDoc.valueChanges();
    this.tutorDetailsDoc.set(tutorDetailsToSave);
  }

  getAllTutorDetails(): Observable<Tutordetails[]> {
    this.tutordetailsCollection = this.db.collection<Tutordetails>('tutordetails');
    return this.tutordetailsCollection.valueChanges();
  }

}
