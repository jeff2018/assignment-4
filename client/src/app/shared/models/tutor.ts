export class Tutor {
    constructor(
        public id: number,
        public tutor_name: string,
        public tutor_subjects: string,
        public topics: string,
        public face_photo: string,
        public first_lesson_date: Date,
        public last_lesson_date: Date,
        public hourly_rates: number,
        public student_count: number,
        public index: number
    ) { }            
}