import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  BsDropdownModule, ModalModule, BsDatepickerModule, TabsModule,
  AccordionModule, AlertModule, ButtonsModule, CarouselModule, CollapseModule,
  PopoverModule, ProgressbarModule, PaginationModule
} from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';

const securityRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
]);

@NgModule({
  imports: [
    CommonModule, SharedModule, securityRouting, ModalModule.forRoot(),
    BsDatepickerModule.forRoot(), TabsModule.forRoot(),
    AccordionModule.forRoot(), AlertModule.forRoot(),
    ButtonsModule.forRoot(), CarouselModule.forRoot(),
    CollapseModule.forRoot(), BsDropdownModule.forRoot(),
    PopoverModule.forRoot(), ProgressbarModule.forRoot(),
    BrowserAnimationsModule, PaginationModule.forRoot()
  ],
  declarations: [LoginComponent, RegisterComponent]
})
export class SecurityModule { }