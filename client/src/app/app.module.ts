import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { HomeComponent } from './home/home.component';
import { ToastyModule } from 'ng2-toasty';
import { NgxLocalStorageModule } from 'ngx-localstorage';
import { TutorModule } from './tutor/tutor.module';
import { LoginComponent } from './security/login/login.component';
import { RegisterComponent } from './security/register/register.component';
import { SecurityModule } from './security/security.module';
import { HeaderComponent, FooterComponent } from './shared/layout';
import { SharedModule } from './shared/shared.module';
import { TutorService } from './shared/services/tutor.service';
import { AuthService } from './shared/services/auth.service';
import { FileuploadService } from './shared/services/fileupload.service';
import { firebaseConfig } from "../environments/firebase.config";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { TutorfirebaseService } from './shared/services/tutorfirebase.service';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
  { path: 'home', component: HomeComponent }
]);

@NgModule({
  declarations: [
    AppComponent, FooterComponent, HeaderComponent
  ],
  imports: [
    BrowserModule, HomeModule, AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, AngularFireAuthModule, AngularFireStorageModule,
    rootRouting, SharedModule, TutorModule,
    NgxLocalStorageModule.forRoot(),
    SecurityModule, ToastyModule
  ],
  providers: [TutorService, AuthService, FileuploadService, TutorfirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }